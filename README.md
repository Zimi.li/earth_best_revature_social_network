# Earth Best Revature Social Network

## Description
In this social network, everyone is friends with everyone. As a user, you will be able to register and login to your account. When you successfully login, you are automatically friends with everyone which means you will see a feed of everyone's posts. Users will be able to create a post and like other people's posts. Users will also have the ability to view other profiles which will display posts of that specific user.

## Technologies
* Apache Tomcat
* Spring Boot
* Spring Data
* Spring MVC
* Docker
* Bootstrap
* Servlet
* Jackson Databind
* Bcrypt
* Junit
* Log4j
* JavaMail
* PostgreSQL
* Mockito
* Sonar Cloud
* Angular 2+
* AWS S3
* AWS EC2
* AWS RDS

## Features

## Getting Started

## Contributor
* Zimi Li: <zimi.li@revature.net>
* Andrew Patrick
* Kevin Dian
* Bhavani Yelagala
